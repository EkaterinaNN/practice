# Код написан студентом группы ИВТ-465 Верещаком Григорием Алексеевичем


from lxml import etree
import re


def get_patent_name(patent_data):
    root = etree.fromstring(patent_data.encode('utf-8'))
    doc_numers = root.findall(".//doc-number")
    countries = root.findall(".//country")
    patent_name = countries[0].text + doc_numers[0].text
    return patent_name

def get_claims(patent_data):
    reg_string = '<claim-ref.*?>|</claim-ref>|<i>|</i>|<b>|</b>|<sup>|</sup>'
    patent_data = re.sub(reg_string , '', patent_data)
    root = etree.fromstring(patent_data.encode('utf-8'))
    claims = root.findall(".//claim-text")
    claims = root.findall(".//claim-text")
    patent_claims = [claim.text for claim in claims]
    return patent_claims
