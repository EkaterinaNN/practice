# Модуль извлечения описаний научно-технических эффектов из патентных документов
------

Разработанный модуль предназначен для поиска наиболее релевантных патентных документов USPTO на основе запросов, сформированных из описаний химических эффектов согласно классификации National Center for Biotechnology Information.

##Состав модуля

```
Effects/                                  описания химических эффектов
main_project/                             файлы проекта
main_project/design.py                    конвертированный файл дизайна
main_project/design.ui                    файл дизайна
main_project/effects_parser.py            парсер химических эффектов
main_project/main.py                      главный модуль проекта
main_project/uspto_parser.py              парсер патентного архива
main_project/xml_parser.py                парсер XML-файлов
test_archives/                            тестовые архивы с патентами
README.md                                 этот файл
requirements.txt                          требования
```
##Что необходимо для корректного запуска и работы модуля
Операционная система Linux.

-  При написании проекта использовался язык программирования **Python**. Для запуска модуля необходимо установить **Python** версии **3.5**.

- **Stanford NLP**. Скачать его можно [отсюда](https://stanfordnlp.github.io/CoreNLP/). 
  
  ```
  В effects_parser:
  path = os.path.abspath(os.curdir) + '/stanford-corenlp-full-2018-02-27/' - путь к 
  Stanford NLP.
  Лучше всего поместить загруженный Stanford CoreNLP в папку с проектом. 
  ```
  
- **Apache Hadoop**. С установкой и запуском можно ознакомиться [здесь](http://www.bogotobogo.com/Hadoop/BigData_hadoop_Install_on_ubuntu_single_node_cluster.php).

- **Apache Spark**. Инструкция по установке и запуску находится [здесь](https://datawookie.netlify.com/blog/2017/07/installing-spark-on-ubuntu/).
    
  ```
  В main.py: 
  findspark.init('/usr/local/spark/') - указывается путь к Apache Spark
  ```
  
- Библиотеки.
   Информация о необходимых библиотеках и их версиях находится в файле _requirements.txt_.

- СУБД **PostgreSQL**.
  Инструкцию по установке и настройке **PostgreSQL** можно найти [здесь](https://losst.ru/ustanovka-postgresql-ubuntu-16-04).
  
  - Информация для подключения к БД.
  
  ```
  user = hduser
  pass = '123456'
  db - hduser
  ```
  
  - Запросы на создание таблиц в БД.
  
  ```
  CREATE TABLE effects (
  effect_id serial PRIMARY KEY,
  description varchar(10000) NOT NULL,
  keywords varchar(10000) NOT NULL
  );
  ```
  
  ```
  CREATE TABLE search_queries (
  query_id serial PRIMARY KEY,
  patent_name varchar(10000) NOT NULL,
  patent_claims varchar(10000000) NOT NULL,
  effect_name varchar(10000) NOT NULL,
  value varchar(10000) NOT NULL
  );
  ```
  
  ```
  CREATE TABLE patents (
  query_id serial PRIMARY KEY,
  patent_name varchar(1000000) NOT NULL,
  patent_claims varchar(1000000) NOT NULL
  );
  ```