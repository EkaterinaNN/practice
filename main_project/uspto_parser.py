# Код написан студентом группы ИВТ-465 Верещаком Григорием Алексеевичем


from pathlib import Path
import shutil
import tarfile
import glob
import zipfile
import os
import re


def extract_archive(file_path):

    """
    Распаковка патентного архива
    Аргументы:
    file_path - путь к патентному архиву в формате .tar
    """
    archive_path = file_path
    archive_name = os.path.splitext(os.path.basename(archive_path))[0]
    dirname, filename = os.path.split(os.path.abspath(archive_path))

    result_dir = dirname + '/patents/' # Каталог,который будет содержать только валидные XML-файлы
    temp_dir = dirname + '/TEMP_' + archive_name
    tar_archive = tarfile.open(archive_path)
    tar_archive.extractall(path=temp_dir)
    tar_archive.close()
    name_pattern = re.compile('[a-zA-Z]+[0-9]+-[0-9]+')
    patents_path = dirname + '/' + archive_name + '_UNZIPPED'
    result_dir = '/home/hduser/Projects/GradProject/main_project/patents/'
    test_name = 'US09551720-20170124-T00001'

    # Распаковать все zip-файлы с патентами в архиве
    for file in glob.iglob(temp_dir + '/**/*.ZIP', recursive=True):
        zip_name = os.path.splitext(os.path.basename(file))[0]
        match = re.match(name_pattern, zip_name)
        if match:
            with zipfile.ZipFile(file, "r") as zip_ref:
                zip_ref.extractall(patents_path)

    for file in glob.iglob(patents_path + '/**/*.XML', recursive=True):
        dirname, filename = os.path.split(os.path.abspath(file))
        if filename.count('-') < 2:
            if not os.path.exists(result_dir):
                os.makedirs(result_dir)
            shutil.move(file, result_dir + filename)
            #print(result_dir + filename)

    shutil.rmtree(temp_dir)
    shutil.rmtree(patents_path)
