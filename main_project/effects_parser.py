# Код написан студентом группы ИВТ-465 Верещаком Григорием Алексеевичем


from stanfordcorenlp import StanfordCoreNLP
import os
import psycopg2


def get_key_words(text):

    """
    Извлечение ключевых слов из текста
    Аргументы:
    text - текст документа, из которого извлекаются слова
    """
    # Подключение к Stanford CoreNLP
    path = os.path.abspath(os.curdir) + '/stanford-corenlp-full-2018-02-27/'
    nlp = StanfordCoreNLP(path)
    necessary_dependencies = ['nmod', 'amod', 'advmod']
    #necessary_dependencies = ['nmod', 'ROOT']
    key_words = []

    # Выделить предложения в тексте
    sentences = text.split('.')
    sentences = sentences[:-1]

    # Для всех предложений выделить ключевые слова
    for sentence in sentences: 
        sentence = sentence + '.'
        current_tokens = nlp.word_tokenize(sentence)
        current_dependencies = nlp.dependency_parse(sentence)
        #print(current_dependencies)
        for current_dependency in current_dependencies:
            if current_dependency[0] in necessary_dependencies:
                key_word = current_tokens[current_dependency[2] - 1].lower()
                if key_word not in key_words:
                    key_words.append(key_word)

    # Закрыть подключение к Stanford CoreNLP
    nlp.close()
    result = []
    for key_word in key_words:
        if key_word[-1] == 's':
            key_word = key_word[0:-1]
        result.append(key_word)
    return result


def parse_single_file(file_path):

    """
    Извлечение из ключевых слов из одного документа
    Аргументы:
    file_path - путь к документу, из которого извлекаются слова
    """

    # Прочитать содержимое файла
    effect_name = os.path.splitext(os.path.basename(file_path))[0]
    file = open(file_path)
    description = file.read()

    # Найти ключевые слова
    key_words = get_key_words(description)
    key_words = ','.join(key_words)
    print('Эффект ' + effect_name + ' добавлен.')
    file.close()

    # Добавить информацию по эффекту в БД химических эффектов
    conn = psycopg2.connect("dbname='hduser' user='hduser' host='localhost' password='123456'")
    cursor = conn.cursor()
    cursor.execute("""INSERT INTO effects (description, keywords, name) VALUES(%s, %s, %s); """, (description, key_words, effect_name))
    conn.commit()
    cursor.close()
    conn.close()

def parse_directory(directory):

    """
    Извлечение ключевых слов из документов в указанной директории
    Аргументы: 
    directory - путь к директории, содержащей документы с описаниями химических эффектов
    """
    directory_with_effects = directory
    files_with_effects = os.listdir(directory_with_effects)

    # Для каждого документа в директории извлечь ключевые слова
    for file_with_effect in files_with_effects:
        #print(file_with_effect)
        parse_single_file(directory_with_effects + file_with_effect)
