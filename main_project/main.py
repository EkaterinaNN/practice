# Код написан студентом группы ИВТ-465 Верещаком Григорием Алексеевичем


import findspark 
# Инициализировать место установки Apache Spark
findspark.init('/usr/local/spark/') 

import pyspark
from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.window import Window
from operator import add
from pyspark.ml.feature import HashingTF, IDF, Tokenizer
from pyspark.ml.feature import StopWordsRemover
from pyspark.sql.functions import regexp_replace, trim, col, lower
from pyspark.sql.types import *
from pyspark.sql.functions import udf
from pyspark.ml.feature import CountVectorizer
from lxml import etree
import re, string
import numpy
import sys
import shutil
import os
from PyQt5 import QtWidgets, QtGui
import psycopg2
import design
import effects_parser
import uspto_parser
import xml_parser
from stanfordcorenlp import StanfordCoreNLP


def removePunctuation(text):

    """
    Удаление пунктуации из текста
    Аргументы:
    text - текст, из которого необходимо удалить пунктуацию
    Возвращаемое значение:
    """
    text = text.lower()
    return re.sub('['+string.punctuation+']', '', text)


def contains_keywords(row, keywords):


    """
    Проверка строки на наличие ключевых слов
    Аргументы:
    row - строка текста, в которой выполняется поиск
    keywords - список ключевых слов
    Возвращаемое значение:
    True - если найдено, False - если не найдено
    """
    result_set = set(row).intersection(set(keywords))
    return True if len(result_set) == len(set(keywords)) else False


def save_rdd(rdd, path):


    """
    Сохранение коллекции RDD в файловую систему
    Аргументы:
    rdd - коллекция RDD
    path - путь к каталогу в файловой системе
    """
    path = '/home/hduser/Projects/GradProject/main_project/rdds/myrdd.txt'
    if os.path.exists(path):
        shutil.rmtree(path)

    rdd.saveAsTextFile(path)



def join_claims(claims): 

    """
    Объединение полей claim-text патента в одну строку
    Аргументы:
    claims - список с содержимым полей claim-text
    Возвращаемое значение:
    строка
    """
    if len(claims) == 0:
        return ' '
    else:
        return ' '.join(str(claim) for claim in claims)


def show_effects(self):


    """
    Вывод загруженных в БД химических эффектов в форму приложения
    """
    conn = psycopg2.connect("dbname='hduser' user='hduser' host='localhost' password='123456'")
    cursor = conn.cursor()
    cursor.execute(""" SELECT * FROM effects """)
    rows = cursor.fetchall()
    cursor.close()
    conn.close()
    if not rows:
        print('БД хим. эффектов пуста')
        self.findBtn.setDisabled(True)
    else:
        self.effectsTable.setRowCount(0)
        self.findBtn.setEnabled(True)
        for row in rows:
            self.effectsList.addItem(row[3])
            rowPosition = self.effectsTable.rowCount()
            self.effectsTable.insertRow(rowPosition)
            self.effectsTable.setItem(rowPosition , 0, QtWidgets.QTableWidgetItem(row[3]))
            self.effectsTable.setItem(rowPosition , 1, QtWidgets.QTableWidgetItem(row[1]))
            self.effectsTable.setItem(rowPosition , 2, QtWidgets.QTableWidgetItem(row[2]))


class MainApp(QtWidgets.QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.patentText.clear()
        self.resultValue.clear()
        self.loadEffectsBtn.clicked.connect(self.load_effects)
        self.expandEffectsBtn.clicked.connect(self.expand_effects)
        self.findBtn.clicked.connect(self.find_patents)
        self.patentsList.itemClicked.connect(self.show_patent)
        self.effectsList.itemClicked.connect(self.clear_fields)
        self.loadArchiveBtn.clicked.connect(self.load_archive)
        show_effects(self)
        if os.path.exists(os.path.abspath(os.curdir) + '/patents/'):
                shutil.rmtree(os.path.abspath(os.curdir) + '/patents/')

    def load_effects(self):

        """
        Загруза химических эффектов в БД эффектов
        """
        directory = QtWidgets.QFileDialog.getExistingDirectory(self, "Выберите директорию с хим. эффектами")

        if directory:
            print('Выполняется загрузка эффектов.')
            effects_parser.parse_directory(directory + '/')
            self.effectsList.clear()
            show_effects(self)
        else:
            QtWidgets.QMessageBox.about(self.centralwidget, "Ошибка", "Не указана директория с химическими эффектами.")

    def expand_effects(self):

        """
        Расширение БД химическиъ эффектов
        """
        file_path = QtWidgets.QFileDialog.getOpenFileName(self, "Выберите файл с описанием хим. эффекта")
        if file_path[0]:
            print('Выполняется добавление эффекта.')
            effects_parser.parse_single_file(file_path[0])
            self.effectsList.clear()
            show_effects(self)
        else:
            QtWidgets.QMessageBox.about(self.centralwidget, "Ошибка", "Не указан путь к файлу с описанием химического эффекта.")

    def load_archive(self):

        """
        Загруза патентного архива
        """
        file_path = QtWidgets.QFileDialog.getOpenFileName(self, "Выберите архив с патентами")
        if file_path[0]:
            uspto_parser.extract_archive(file_path[0])
            print('Выполняется загрузка патентов.')

            #Работа с RDD
            patents_dir = os.path.abspath(os.curdir) + '/patents/'
            print('Путь к каталогу с xml-файлами: ' + patents_dir)
            input_rdd = sc.wholeTextFiles(patents_dir + '*')
            print('Загружено патентов в RDD: ' + str(input_rdd.count()))

            print('Выполняется извлечение ключевых признаков из текстов патентов.')
            rdd_with_separated_claims = input_rdd.map(lambda x: (xml_parser.get_patent_name(x[1]), xml_parser.get_claims(x[1])))
            rdd_with_joined_claims = rdd_with_separated_claims.map(lambda x: (x[0], join_claims(x[1])))
            prepared_input_rdd = rdd_with_joined_claims.map(lambda x: (x[0], x[1], removePunctuation(x[1])))

            input_data = prepared_input_rdd.toDF()
            patent_data = input_data.selectExpr('_1 as patent_name', '_2 as patent_claims', '_3 as prepared_claims')

            tokenizer = Tokenizer(inputCol="prepared_claims", outputCol="words")
            words_data = tokenizer.transform(patent_data)

            remover = StopWordsRemover(inputCol="words", outputCol="filtered")
            prepared = remover.transform(words_data)

            vectorizer = CountVectorizer(inputCol="filtered", outputCol="raw_features").fit(prepared)

            featurized_data = vectorizer.transform(prepared)
            featurized_data.cache()

            idf = IDF(inputCol='raw_features', outputCol='features')
            idf_model = idf.fit(featurized_data)
            rescaled_data = idf_model.transform(featurized_data)

            useful_col_names = ['patent_name', 'patent_claims','filtered','raw_features']
            result_data = rescaled_data['patent_name', 'patent_claims','filtered','features']
            print('Выделенные ключевые признаки в патентныхдокументах:')
            result_data.show()

            global vocabulary
            global rdd_with_tfidf
            rdd_with_tfidf = result_data.rdd
            print(os.path.abspath(os.curdir))
            save_rdd(rdd_with_tfidf, os.path.abspath(os.curdir) +'/rdds/rdd.txt')
            print('RDD с описанием патентов успешно сохранена в файловую систему')

            self.tableWidget.setRowCount(0)
            for x in result_data.collect():
                rowPosition = self.tableWidget.rowCount()
                self.tableWidget.insertRow(rowPosition)
                self.tableWidget.setItem(rowPosition , 0, QtWidgets.QTableWidgetItem(x[0]))
                self.tableWidget.setItem(rowPosition , 1, QtWidgets.QTableWidgetItem(x[1]))
            vocabulary = vectorizer.vocabulary
        else:
            QtWidgets.QMessageBox.about(self.centralwidget, "Ошибка", "Не указан путь к патентному архиву.")

    def find_patents(self):

        """
        Поиск релевантных патентов
        """
        self.patentsList.clear()
        global rdd_with_tfidf
        if self.tableWidget.rowCount():
            global vocabulary
            if self.effectsList.currentItem() != None:
                selected_effect = self.effectsList.currentItem().text()
                conn = psycopg2.connect("dbname='hduser' user='hduser' host='localhost' password='123456'")
                cursor = conn.cursor()
                cursor.execute(""" SELECT * FROM effects WHERE name = %s""", [selected_effect])
                row = cursor.fetchone()
                cursor.close()
                conn.close()
                effect_name = row[3]
                keywords = row[2].split(',')
                result_data = rdd_with_tfidf.map(lambda x: (x[0], x[1], x[2], x[3].toArray().tolist()))


                filtered_data = result_data.filter(lambda x: contains_keywords(x[2], keywords))
                if filtered_data.count() == 0:
                    print('Не найдено подходящих патентов')
                    QtWidgets.QMessageBox.about(self.centralwidget, "Результат поиска", "Патенты не найдены.")
                else:
                    print('Найдено подходящих патентов: ' + str(filtered_data.count()))
                    conn = psycopg2.connect("dbname='hduser' user='hduser' host='localhost' password='123456'")
                    cursor = conn.cursor()
                    cursor.execute("""TRUNCATE search_queries;""")
                    conn.commit()
                    cursor.close()
                    conn.close()

                    for x in filtered_data.collect():
                        result_value = 0
                        for keyword in keywords:
                            result_value = result_value + x[3][vocabulary.index(keyword)]

                        conn = psycopg2.connect("dbname='hduser' user='hduser' host='localhost' password='123456'")
                        cursor = conn.cursor()
                        cursor.execute("""INSERT INTO search_queries(patent_name, patent_claims, effect_name, value) VALUES(%s, %s, %s, %s); """, (x[0], x[1], effect_name, result_value))
                        conn.commit()
                        cursor.close()
                        conn.close()

                    conn = psycopg2.connect("dbname='hduser' user='hduser' host='localhost' password='123456'")
                    cursor = conn.cursor()
                    cursor.execute(""" SELECT * FROM search_queries ORDER BY value""")
                    rows = cursor.fetchall()
                    cursor.close()
                    conn.close()
                    #print(len(rows))
                    self.patentsList.clear()
                    for row in reversed(rows):
                        self.patentsList.addItem(row[1])

            else:
                QtWidgets.QMessageBox.about(self.centralwidget, "Ошибка", "Химический эффект не выбран.") 
        else:
            QtWidgets.QMessageBox.about(self.centralwidget, "Ошибка", "Нет загруженных патентов.") 


    def show_patent(self):

        """
        Показ текста патента и его релевантности
        """
        self.patentText.clear()
        self.resultValue.clear()
        selected_patent = self.patentsList.currentItem().text()

        conn = psycopg2.connect("dbname='hduser' user='hduser' host='localhost' password='123456'")
        cursor = conn.cursor()
        cursor.execute(""" SELECT * FROM search_queries WHERE patent_name = %s""", [selected_patent])
        row = cursor.fetchone()
        cursor.close()
        conn.close()
        self.patentText.setText(row[2])
        self.resultValue.setText(row[4])

    def clear_fields(self):

        """
        Очистка полей в главном окне приложения
        """
        self.patentText.clear()
        self.resultValue.clear()
        self.patentsList.clear()


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = MainApp()
    window.setWindowTitle('Модуль извлечения описаний научно-технических эффектов')
    window.show()
    app.exec_()


if __name__ == '__main__':
    sc = SparkContext()
    nlp = StanfordCoreNLP(r'/home/hduser/Projects/GradProject/stanford-corenlp-full-2018-02-27/')
    sqlContext = SQLContext(sc)
    main()
    nlp.close()
    sc.stop()